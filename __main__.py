from random import choice
from termo import Termo, Feedback, Result, InvalidAttempt
from termcolor import colored

def to_output(result: Result):
    for c, feedback in result.feedback:
        if feedback == Feedback.RIGHT_PLACE:
            print(colored(c, "green"), end=' ')
        elif feedback == Feedback.WRONG_PLACE:
            print(colored(c, "yellow"), end=' ')
        else:
            print('_', end=' ')
    print()

def main():
    try:
        with open('palavras.txt') as input_stream:
            words = list(filter(lambda it: len(it) == 5 and it.isalpha(), map(str.strip, input_stream.readlines())))
            if not words:
                print("Nenhuma palavra válida encontrada no arquivo 'palavras.txt'. Certifique-se de que o arquivo contenha palavras de 5 letras.")
                return
    except FileNotFoundError:
        print("Arquivo 'palavras.txt' não encontrado. Por favor, crie este arquivo com uma lista de palavras de 5 letras.")
        return

    word = choice(words)
    limit = 5
    counter = 0
    termo = Termo(word, words)
    result = Result(win=False, feedback=None)
    print('Tente adivinhar a palavra')

    while not result.win and counter < limit:
        guess = input(f'Tentativa {counter+1}: ')
        if len(guess) != 5 or not guess.isalpha():
            print("Por favor, insira uma palavra válida de 5 letras.")
            continue

        try:
            result = termo.test(guess)
            to_output(result)
            counter += 1
        except InvalidAttempt:
            print("Tentativa inválida. Por favor, tente novamente.")

    if result.win:
        print('Parabéns! Você adivinhou a palavra.')
    else:
        print(f'Game over. A palavra correta era: {word}')

if __name__ == '__main__':
    main()

from django.contrib import admin
from termo.models import Profile

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'score', 'created_at')
    search_fields = ('user__username',)
    list_filter = ('created_at',)
    ordering = ('-created_at',)
    fields = ('user', 'score', 'created_at')
    readonly_fields = ('created_at',)

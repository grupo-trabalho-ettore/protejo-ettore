from django.db import models
from django.conf import settings
from datetime import timedelta
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.utils import timezone

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.user.username



@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def on_create_user(instance, created, raw, **kwargs):
    if raw:
        return
    if not created:
        return
    Profile.objects.create(user=instance)


class Word(models.Model):
    content = models.CharField(
        max_length=5,
    )


class Pool(models.Model):

    datetime = models.DateTimeField(

    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
    )

    word = models.ForeignKey(
        Word,
        on_delete=models.CASCADE,
    )


def get_word_of_day(date):
    try:
        return Pool.objects.filter(
            datetime__lte=date + timedelta(days=1),
        ).get()
    except:
        word, _ = Word.objects.get_or_create(content='teste')
        return Pool.objects.create(
            word=word,
            datetime=date,
        )

import os
from datetime import timedelta
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from termo.models import get_word_of_day, Profile, Word, Pool
from termo.termo import Termo, Feedback, InvalidAttempt

class WordTest(TestCase):
    def test_get_word_of_day_is_not_none(self):
        now = timezone.now()
        word = get_word_of_day(now)
        self.assertIsNotNone(word)

    def test_is_the_same_word(self):
        now = timezone.now()
        word1 = get_word_of_day(now)
        word2 = get_word_of_day(now)
        self.assertEqual(word1, word2)

class ProfileModelTest(TestCase):
    def test_profile_creation_on_user_creation(self):
        user = User.objects.create_user(username='testuser', password='password')
        profile = Profile.objects.get(user=user)
        self.assertTrue(Profile.objects.filter(user=user).exists())
        self.assertEqual(profile.user, user)
        self.assertEqual(profile.score, 0)

    def test_profile_str_representation(self):
        user = User.objects.create_user(username='testuser')
        profile = Profile(user=user)
        self.assertEqual(str(profile), 'testuser')

class TermoTest(TestCase):
    def setUp(self):
        self.word = Termo(word="test", valid_words={"test", "trial", "experiment"})

    def test_initialization(self):
        self.assertEqual(self.word.word, "test")
        self.assertIn("test", self.word.valid_words)

    def test_feedback(self):
        feedback_list = list(self.word.feedback("test"))
        expected_feedback = [(char, Feedback.RIGHT_PLACE) for char in "test"]
        self.assertEqual(feedback_list, expected_feedback)

    def test_invalid_attempt(self):
        with self.assertRaises(InvalidAttempt):
            self.word.feedback("wrong")

    def test_win_condition(self):
        result = self.word.test("test")
        expected_feedback = [(char, Feedback.RIGHT_PLACE) for char in "test"]
        self.assertTrue(result.win)
        self.assertEqual(result.feedback, expected_feedback)

class PoolModelTest(TestCase):
    def test_create_pool_entry(self):
        now = timezone.now()
        pool_entry = get_word_of_day(now)
        self.assertIsNotNone(pool_entry)
        self.assertTrue(Pool.objects.filter(datetime=now).exists())

    def test_pool_word_association(self):
        word = Word.objects.create(content="test")
        pool_entry = Pool.objects.create(word=word, datetime=timezone.now())
        self.assertEqual(pool_entry.word.content, "test")

class TermoViewsTest(TestCase):
    def setUp(self):
        self.file_path = os.path.join(os.path.dirname(__file__), '../protejo-ettore/palavras_filtradas.txt')

    def test_index_view_status_code(self):
        url = reverse('termo:index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_index_view_contains_correct_html(self):
        url = reverse('termo:index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_palavras_filtradas_view_file_not_found(self):
        backup_path = self.file_path + '.bak'
        try:
            if os.path.exists(self.file_path):
                os.rename(self.file_path, backup_path)

            url = reverse('termo:palavras_filtradas')
            response = self.client.get(url)
            self.assertEqual(response.status_code, 404)
        finally:
            if os.path.exists(backup_path):
                os.rename(backup_path, self.file_path)


from django.shortcuts import render
import os
from django.http import JsonResponse

def index_view(request):
    return render(request, 'index.html')

def palavras_filtradas(request):
    file_path = os.path.join(os.path.dirname(__file__), '..', 'palavras_filtradas.txt')
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            words = [line.strip() for line in file if line.strip()]
        return JsonResponse(words, safe=False)
    except FileNotFoundError:
        return JsonResponse({'error': 'File not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)
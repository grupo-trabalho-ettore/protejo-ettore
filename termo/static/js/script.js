'use strict';
const header_filter = document.querySelector('.header');
const BACKSPACE_KEY = 'Backspace';
const ENTER_KEY = 'Enter';
let WORD_LIST = [];
let WORD_OF_THE_DAY = '';
const MAX_NUMBER_OF_ATTEMPTS = 6;
const history = [];
let currentWord = '';

const fetchWords = async () => {
  try {
    const response = await fetch('static/palavras_filtradas.txt');
    if (!response.ok) {
      throw new Error('Erro ao carregar palavras');
    }
    const text = await response.text();
    const words = text.split('\n').map(word => word.trim()).filter(word => word.length > 0);
    return words;
  } catch (error) {
    console.error('Erro ao carregar palavras:', error);
    return [];
  }
};

const init = async () => {
  WORD_LIST = await fetchWords();
  if (WORD_LIST.length > 0) {
    WORD_OF_THE_DAY = WORD_LIST[getRandomIndex(WORD_LIST.length)];
    console.log('Palavra do dia:', WORD_OF_THE_DAY); // Added line to print the word
  } else {
    console.error('Lista de palavras está vazia.');
    showMessage('Erro ao carregar palavras.');
    return;
  }

  console.log(' Bem vindo ao Termo-Ettore');

  const KEYBOARD_KEYS = ['QWERTYUIOP', 'ASDFGHJKL', 'ZXCVBNM'];

  const gameBoard = document.querySelector('#board');
  const keyboard = document.querySelector('#keyboard');

  generateBoard(gameBoard);
  generateBoard(keyboard, 3, 10, KEYBOARD_KEYS, true);

  document.addEventListener('keydown', event => onKeyDown(event.key));
  gameBoard.addEventListener('animationend', event => event.target.setAttribute('data-animation', 'idle'));
  keyboard.addEventListener('click', onKeyboardButtonClick);
};

const showMessage = (message) => {
  const toast = document.createElement('li');
  toast.textContent = message;
  toast.className = 'toast';
  document.querySelector('.toaster ul').prepend(toast);
  setTimeout(() => toast.classList.add('fade'), 1000);
  toast.addEventListener('transitionend', (event) => event.target.remove());
};

const checkGuess = (guess, word) => {
  const guessLetters = guess.split('');
  const wordLetters = word.split('');
  const remainingWordLetters = [];
  const remainingGuessLetters = [];
  const modalwin = document.querySelector('.modal-win');

  const currentRow = document.querySelector(`#board ul[data-row='${history.length}']`);

  currentRow.querySelectorAll('li').forEach((element, index) => {
    element.setAttribute('data-status', 'none');
    element.setAttribute('data-animation', 'flip');
    element.style.animationDelay = `${index * 300}ms`;
    element.style.transitionDelay = `${index * 400}ms`;
  });

  wordLetters.forEach((letter, index) => {
    if (guessLetters[index] === letter) {
      currentRow.querySelector(`li:nth-child(${index + 1})`).setAttribute('data-status', 'valid');
      document.querySelector(`[data-key='${letter}']`).setAttribute('data-status', 'valid');
      remainingWordLetters.push(false);
      remainingGuessLetters.push(false);
    } else {
      remainingWordLetters.push(letter);
      remainingGuessLetters.push(guessLetters[index]);
    }
  });

  remainingWordLetters.forEach(letter => {
    if (letter === false) return;
    if (remainingGuessLetters.indexOf(letter) !== -1) {
      const column = currentRow.querySelector(`li:nth-child(${remainingGuessLetters.indexOf(letter) + 1})`);
      column.setAttribute('data-status', 'invalid');
      const keyboardKey = document.querySelector(`[data-key='${letter}']`);
      if (keyboardKey.getAttribute('data-status') !== 'valid') {
        keyboardKey.setAttribute('data-status', 'invalid');
      }
    }
  });

  guessLetters.forEach(letter => {
    const keyboardKey = document.querySelector(`[data-key='${letter}']`);
    if (keyboardKey.getAttribute('data-status') === 'empty') {
      keyboardKey.setAttribute('data-status', 'none');
    }
  });

  history.push(currentWord);
  currentWord = '';

  if (guess === word) {
      modalwin.style.display = 'flex';
      header_filter.style.filter = 'blur(2px)';
  }
};



const onKeyboardButtonClick = (event) => {
  if (event.target.nodeName === 'LI') {
    onKeyDown(event.target.getAttribute('data-key'));
  }
};

const onKeyDown = (key) => {
  if (history.length >= MAX_NUMBER_OF_ATTEMPTS) return;
  const currentRow = document.querySelector(`#board ul[data-row='${history.length}']`);
  let targetColumn = currentRow.querySelector('[data-status="empty"]');

  if (key === BACKSPACE_KEY) {
    if (targetColumn === null) {
      targetColumn = currentRow.querySelector('li:last-child');
    } else {
      targetColumn = targetColumn.previousElementSibling ?? targetColumn;
    }
    targetColumn.textContent = '';
    targetColumn.setAttribute('data-status', 'empty');
    currentWord = currentWord.slice(0, -1);
    return;
  }

  if (key === ENTER_KEY) {
    if (currentWord.length < 5) {
      showMessage('Não acha que está faltando algumas letras?');
      return;
    }
    if (currentWord.length === 5 && WORD_LIST.includes(currentWord)) {
      checkGuess(currentWord, WORD_OF_THE_DAY);
      
    } else {
      currentRow.setAttribute('data-animation', 'invalid');
      showMessage('Isso não é uma palavra de verdade, não é?');
    }
    return;
    
  }

  if (currentWord.length >= 5) return;

  const upperCaseLetter = key.toUpperCase();
  if (/^[A-Z]$/.test(upperCaseLetter)) {
    currentWord += upperCaseLetter;
    targetColumn.textContent = upperCaseLetter;
    targetColumn.setAttribute('data-status', 'filled');
    targetColumn.setAttribute('data-animation', 'pop');
  }
};

const generateBoard = (board, rows = 6, columns = 5, keys = [], keyboard = false) => {
  for (let row = 0; row < rows; row++) {
    const elmRow = document.createElement('ul');
    elmRow.setAttribute('data-row', row);
    for (let column = 0; column < columns; column++) {
      const elmColumn = document.createElement('li');
      elmColumn.setAttribute('data-status', 'empty');
      elmColumn.setAttribute('data-animation', 'idle');
      if (keyboard && keys.length > 0) {
        const key = keys[row].charAt(column);
        elmColumn.textContent = key;
        elmColumn.setAttribute('data-key', key);
      }
      if (keyboard && elmColumn.textContent === '') continue;
      elmRow.appendChild(elmColumn);
    }
    board.appendChild(elmRow);
  }
  if (keyboard) {
    const enterKey = document.createElement('li');
    enterKey.setAttribute('data-key', ENTER_KEY);
    enterKey.textContent = ENTER_KEY;
    board.lastChild.prepend(enterKey);
    const backspaceKey = document.createElement('li');
    backspaceKey.setAttribute('data-key', BACKSPACE_KEY);
    backspaceKey.textContent = BACKSPACE_KEY;
    board.lastChild.append(backspaceKey);
  }
};

document.addEventListener('DOMContentLoaded', init);

function getRandomIndex(maxLength) {
  return Math.floor(Math.random() * Math.floor(maxLength));
}

// ==============================
// Abrir modal help 
const help = document.querySelector('.help');
const modalHelp = document.querySelector('.modal-help');
const body = document.getElementById('corpo')



help.addEventListener('click', function () {
  modalHelp.style.display = 'flex';
  header_filter.style.filter = 'blur(2px)';
});

// Fechar modal help, clicando no body
body.addEventListener('click', function () {
  modalHelp.style.display = 'none';
  header_filter.style.filter = 'none';
});

// ==============================
// Abrir modal menu 
const menu = document.querySelector('.menu');
const modalMenu = document.querySelector('.modal-menu');
const body_bg = document.querySelector('.bg');


menu.addEventListener('click', function () {
  modalMenu.style.display = 'flex';
  header_filter.style.filter = 'blur(2px)';
});

// Fechar modal menu, clicando no body
body.addEventListener('click', function () {
  modalMenu.style.display = 'none';
  header_filter.style.filter = 'none';
});

// ==============================
// Modal info
const info = document.querySelector('.info');
const modalInfo = document.querySelector('.modal-info');



info.addEventListener('click', function () {
  modalInfo.style.display = 'flex';
  header_filter.style.filter = 'blur(2px)';
});

// Fechar modal help, clicando no body
body.addEventListener('click', function () {
  modalInfo.style.display = 'none';
  header_filter.style.filter = 'none';
});

// ==============================
// Dark-mode
document.addEventListener("DOMContentLoaded", function () {
  const darkModeToggle = document.getElementById("darkmode-toggle");

  darkModeToggle.addEventListener("change", function () {
    if (this.checked) {
      document.body.classList.add("dark-mode");
    } else {
      document.body.classList.remove("dark-mode");
    }
  });
});

const checkbox = document.getElementById('darkmode-toggle');
const header = document.querySelector('.header');

let backgroundImage = body_bg.style.backgroundImage;

checkbox.addEventListener('change', function () {
  if (this.checked) {
    body_bg.style.transition = 'background-color 0.5s'; // Adiciona transição para mudança de cor
    header.style.transition = 'background 0.5s'; // Adiciona transição para mudança de cor
    menu.style.transition = 'filter 0.5s'; // Adiciona transição para mudança de brilho
    help.style.transition = 'filter 0.5s'; // Adiciona transição para mudança de brilho
    info.style.transition = 'filter 0.5s';


    body_bg.style.backgroundColor = '#f5f5f5'; // bg branco
    header.style.background = 'rgb(255,255,255)';
    header.style.background = 'linear-gradient(0deg, rgba(90,206,173,0.4) 0%, rgba(255,255,255,0.1) 100%)';
    menu.style.filter = 'brightness(100%)';
    help.style.filter = 'brightness(100%)';
    info.style.filter = 'brightness(100%)';

  } else {
    body_bg.style.transition = 'background-color 0.5s'; // Adiciona transição para mudança de cor
    header.style.transition = 'background 0.5s'; // Adiciona transição para mudança de cor
    menu.style.transition = 'filter 0.5s'; // Adiciona transição para mudança de brilho
    help.style.transition = 'filter 0.5s'; // Adiciona transição para mudança de brilho
    info.style.transition = 'filter 0.5s'; // Adiciona transição para mudança de brilho

    body_bg.style.backgroundColor = '#131313'; // bg preto
    header.style.background = 'rgb(109,152,134)';
    header.style.background = 'linear-gradient(0deg, rgba(109,152,134,0.9) 19%, rgba(104,104,104,0.5) 100%)';
    menu.style.filter = 'brightness(35%)';
    help.style.filter = 'brightness(35%)';
    info.style.filter = 'brightness(35%)';
  }

  body_bg.style.backgroundImage = backgroundImage;
});
from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.shortcuts import render
from . import views
from .views import palavras_filtradas

app_name = 'termo'

urlpatterns = [
    path('palavras_filtradas/', palavras_filtradas, name='palavras_filtradas'),
    path('', views.index_view, name='index'),
]